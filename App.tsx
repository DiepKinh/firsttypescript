import React from 'react';
import {AuthUserProvider} from './src/context/AuthUserProvider';
import Login from './src/screens/Login';
import {AppRouter} from './src/stacks/AppRouter';

const App = () => {
  return (
    <AuthUserProvider>
      <AppRouter />
    </AuthUserProvider>
  );
};
export default App;
