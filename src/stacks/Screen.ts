import Home from '../screens/Home';
import About from '../screens/About';
import BottomTabStack from './BottomTabStack';
import DrawerStack from './DrawerStack';
import TabIcon from './TabIcon';

export type Screen = {
  name: string;
  component: any;
  options?: any;
};

export const MAIN_STACK_SCREEN: Screen[] = [
  {
    name: 'MainDrawer',
    component: DrawerStack,
  },
  {
    name: 'MainTab',
    component: BottomTabStack,
  },
];

export const BOTTOM_TAB_STACK_SCREEN: Screen[] = [
  {
    name: 'Home',
    component: Home,
    options: {
      tabBarIcon: ({color,size}) => {
        return TabIcon('home', color, size);
      },
    },
  },
  {
    name: 'About',
    component: About,
    options: {
      tabBarIcon: ({color, size}) => {
        return TabIcon('cog', color, size);
      },
    },
  },
];

export const DRAWER_STACK_SCREEN: Screen[] = [
  {
    name: 'Home',
    component: Home,
  },
  {
    name: 'About',
    component: About,
  },
];
