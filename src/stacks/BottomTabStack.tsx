import * as React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {MainStackParamList} from './Navigation';
import {BOTTOM_TAB_STACK_SCREEN} from './Screen';

const Tab = createBottomTabNavigator<MainStackParamList>();

export default function BottomTabStack() {
  return (
    <Tab.Navigator>
      {BOTTOM_TAB_STACK_SCREEN.map(item => (
        <Tab.Screen
          key={item.name}
          name={item.name as keyof MainStackParamList}
          component={item.component}
          options={item.options}
        />
      ))}
    </Tab.Navigator>
  );
}
