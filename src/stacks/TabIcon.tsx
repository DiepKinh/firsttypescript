import * as React from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export default function TabIcon(name: string, color: string, size: number) {
  return <MaterialCommunityIcons name={name} color={color} size={size} />;
}
