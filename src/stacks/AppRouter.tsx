import {NavigationContainer} from '@react-navigation/native';
import React, {useContext} from 'react';
import {AuthUserContext} from '../context/AuthUserProvider';
import LoginStack from './LoginStack';
import {Menu} from '../screens/Menu';

export const AppRouter = () => {
  const auth = useContext(AuthUserContext);
  return (
    <NavigationContainer>
      {auth.isAuth ? <Menu /> : <LoginStack />}
    </NavigationContainer>
  );
};
