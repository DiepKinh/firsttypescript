import React, {useContext, useEffect} from 'react';
import {Alert, Button, Text, View} from 'react-native';
import {AuthUserContext} from '../context/AuthUserProvider';
import {MainStackScreenProps} from '../stacks/Navigation';

const Home: React.FC<MainStackScreenProps<'Home'>> = ({navigation, route}) => {
  const auth = useContext(AuthUserContext);
  useEffect(() => {
    Alert.alert(
      'Chào mừng bạn ' + auth.isName + ' đã đến với \n Yolo system',
      '',
      [{text: 'OK'}],
    );
  }, []);
  return <View>Home</View>;
};
export default Home;
