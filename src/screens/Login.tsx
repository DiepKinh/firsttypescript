/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Alert,
} from 'react-native';
import {MyTextInput} from '../components/MyTextInput';
import {MyButton} from '../components/MyButton';
import {AuthUserContext} from '../context/AuthUserProvider';

type textProp = {
  text: string;
};
const TextCom: React.FC<textProp> = ({text}) => {
  return <Text style={styles.text}>{text}</Text>;
};

const Login = () => {
  const auth = useContext(AuthUserContext);
  const [userName, setUserName] = useState<string>();
  const [passWord, setPassWord] = useState<string>();
  const [errorUserName, setErrorUserName] = useState<string>();
  const [errorPassWord, setErrorPassWord] = useState<string>();

  useEffect(() => {
    Alert.alert('Chào mừng bạn đã đến với \n Yolo system', '', [{text: 'OK'}]);
  }, []);

  const isValidField = useCallback((value?: string) => {
    return !!value && value.length > 0;
  }, []);

  useEffect(() => {
    if (isValidField(userName)) {
      setErrorUserName(undefined);
    }
  }, [userName, isValidField]);

  useEffect(() => {
    if (isValidField(passWord)) {
      setErrorPassWord(undefined);
    }
  }, [passWord, isValidField]);

  const validateUser = useCallback(() => {
    console.log('validateUser', userName);
    if (isValidField(userName)) {
      setErrorUserName(undefined);
      return true;
    } else {
      setErrorUserName('Bạn phải nhập tên đăng nhập');
      return false;
    }
  }, [isValidField, userName]);

  const validatePassword = useCallback(() => {
    if (isValidField(passWord)) {
      setErrorPassWord(undefined);
      return true;
    } else {
      setErrorPassWord('Bạn phải nhập mật khẩu');
      return false;
    }
  }, [isValidField, passWord]);

  const validateLogin = useCallback(() => {
    const isValidUser = validateUser();
    const isValidPassword = validatePassword();
    if (isValidUser && isValidPassword) {
      Alert.alert('Xin chào', userName, [
        {
          text: 'OK',
          onPress: () => {
            console.log('OK');
            auth.setAuth?.(true);
            auth.setName?.(userName);
          },
        },
      ]);
    }
  }, [auth, userName, validatePassword, validateUser]);

  const isValidLogin = useMemo(() => {
    return isValidField(userName) && isValidField(passWord);
  }, [isValidField, passWord, userName]);

  const loginButtonStyle = useMemo(
    () =>
      isValidLogin
        ? styles.button
        : {...styles.button, backgroundColor: '#EEEEEE'},
    [isValidLogin],
  );
  return (
    <SafeAreaView style={{backgroundColor: 'white'}}>
      <StatusBar barStyle="dark-content" backgroundColor="white" />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={{backgroundColor: 'white'}}>
        <View style={styles.view}>
          <TextCom text="Yolo System" />
          <MyTextInput
            style={styles.textInput}
            placeholder="Tên đăng nhập"
            value={userName}
            onChangeText={setUserName}
            error={errorUserName}
            onBlur={validateUser}
          />
          <MyTextInput
            style={styles.textInput}
            placeholder="Mật khẩu"
            secureTextEntry //dùng để bảo mật pw khi gõ
            value={passWord}
            onChangeText={setPassWord}
            error={errorPassWord}
            onBlur={validatePassword}
          />
          <MyButton style={loginButtonStyle} onPress={validateLogin} />
          <TextCom text="Or" />
          <MyButton
            style={[styles.button, {backgroundColor: 'green'}]}
            textBtn="Facebook"
          />
          <MyButton
            style={[styles.button, {backgroundColor: 'red'}]}
            textBtn="Google"
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  text: {
    fontSize: 50,
    color: 'green',
    fontWeight: 'bold',
    marginTop: 5,
  },
  textInput: {
    width: 400,
    height: 40,
    borderColor: '#9098B1',
    borderRadius: 10,
    borderWidth: 1,
    paddingLeft: 15,
    marginTop: 15,
  },
  button: {
    width: '96%',
    height: 40,
    alignItems: 'center',
    borderRadius: 30,
    marginTop: 20,
    justifyContent: 'center',
    backgroundColor: 'gray',
  },
  view: {
    marginTop: 120,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#9098B1',
    borderRadius: 10,
    borderWidth: 1,
    backgroundColor: 'white',
    paddingBottom: 20,
  },
});

export default Login;
