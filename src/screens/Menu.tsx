import React, {useState} from 'react';
import {Button, StyleSheet, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Header} from 'react-native/Libraries/NewAppScreen';
import MyButton from '../components/MyButton';
// import MainStack from './MainStack';

export type StackType = 'DrawerStack' | 'BottomBarStack';
export const Menu = () => {
  const [stack, setStack] = useState<StackType>();

  return (
    <SafeAreaView style={styles.view}>
      <MyButton
        style={styles.button}
        textBtn="Drawer"
        onPress={() => setStack('DrawerStack')}
      />
      <MyButton
        style={styles.button}
        textBtn="BottomBar"
        onPress={() => setStack('BottomBarStack')}
      />
    </SafeAreaView>
  );
  //    : (
  //     <MainStack stackType={stack} />
  //   );
};

const styles = StyleSheet.create({
  view: {
    width: '100%',
    height: '100%',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: 350,
    height: 40,
    alignItems: 'center',
    borderRadius: 30,
    marginTop: 20,
    justifyContent: 'center',
    backgroundColor: 'green',
  },
});
