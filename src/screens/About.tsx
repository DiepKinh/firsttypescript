import React from 'react';
import {Text, View} from 'react-native';
import {MainStackScreenProps} from '../stacks/Navigation';

const About: React.FC<MainStackScreenProps<'About'>> = ({
  navigation,
  route,
}) => {
  return (
    <View>
      <Text>Yolo system version 1.0</Text>
    </View>
  );
};
export default About;
