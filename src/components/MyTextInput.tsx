import React from 'react';
import {StyleSheet, Text, TextInput, TextInputProps, View} from 'react-native';

export type Props = TextInputProps & {
  error?: string;
};
export const MyTextInput: React.FC<Props> = ({error, ...others}) => {
  return (
    <View>
      <TextInput {...others} />
      {error && <Text style={styles.textError}>{error}</Text>}
    </View>
  );
};

const styles = StyleSheet.create({
  textError: {
    color: 'red',
    fontSize: 14,
  },
});

export default MyTextInput;
