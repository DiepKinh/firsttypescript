import React from 'react';
import {
  StyleSheet,
  Text,
  TextStyle,
  TouchableOpacity,
  TouchableOpacityProps,
} from 'react-native';

export type buttonProp = TouchableOpacityProps & {
  textBtn?: string;
  textStyle?: TextStyle;
};

export const MyButton: React.FC<buttonProp> = ({
  textBtn = 'Login',
  textStyle = styles.textBtn,
  ...others
}) => {
  return (
    <TouchableOpacity {...others}>
      <Text style={textStyle}>{textBtn}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  textBtn: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'black',
  },
});

export default MyButton;
